# Changelog
## v0.5 (2016-12-19)
* New application: TokenRing tolerating fail-stop-failures.
* Helper class for ring topologies.

## v0.4 (2016-12-05)
* New application: Berkley time synchronisation.
* Processes can fail after a given number of seconds.
* Minor bug fixes.
* Unit tests against PyPy and the soon-to-be-released Python 3.6.

## v0.3 (2016-11-29)
* Inputactions can now react on any condition (e.g. variable changes) within 1 msec, not only arriving messages.
* Added a simple Timeout class.

## v0.2 (2016-11-23)
* Do not relay messages via the algorithm process, but send them directly.
* Send Nil messages to all nodes, not just those in N0.
* New directory structure.
* Improved tests.

## v0.1 (2016-11-19)
* Initial release.