import unittest
from unittest import mock

import time

from async_algorithm.algorithm import Algorithm
from async_algorithm.message import Nil
from async_algorithm.utils import Ring, Timeout
from task_4.tokenring import TokenRingNode, Token, TokenSent, TokenCreated


class TokenRingTestCase(unittest.TestCase):
    @staticmethod
    def _wait_and_loop(e):
        time.sleep(0.1)
        e.loop()

    def setUp(self):
        self.algo = Algorithm()

        self.ring = Ring(5, 'e')

        self.e1 = TokenRingNode(str(self.ring[1]), self.ring[1], n0=True)
        self.e2 = TokenRingNode(str(self.ring[2]), self.ring[2])
        self.e3 = TokenRingNode(str(self.ring[3]), self.ring[3])
        self.e4 = TokenRingNode(str(self.ring[4]), self.ring[4])
        self.e5 = TokenRingNode(str(self.ring[5]), self.ring[5])

        self.algo.add_element(self.e1)
        self.algo.add_element(self.e2)
        self.algo.add_element(self.e3)
        self.algo.add_element(self.e4)
        self.algo.add_element(self.e5)

        for pos in self.ring:
            self.algo.add_connection(str(pos), str(pos + 1))
            self.algo.add_connection(str(pos), str(pos + 2))
            self.algo.add_connection(str(pos), str(pos - 2))

    def test_inputaction1(self):
        self.e1.inqueue.put(Nil(dst='e1'))
        self._wait_and_loop(self.e1)

        self.assertEqual(
            self.e2.inqueue.get(timeout=1), Token(src='e1', dst='e2')
        )
        self.assertEqual(
            self.e3.inqueue.get(timeout=1), TokenSent(src='e1', dst='e3')
        )

    def test_inputaction2(self):
        self.assertIsNone(self.e3.timer._expires)
        self.e3.inqueue.put(TokenSent(src='e1', dst='e3'))
        self._wait_and_loop(self.e3)
        self.assertIsNotNone(self.e3.timer._expires)

    def test_inputaction3(self):
        self.assertTrue(self.e1.next_alive)
        self.e1.inqueue.put(TokenCreated(src='e3', dst='e1'))
        self._wait_and_loop(self.e1)
        self.assertFalse(self.e1.next_alive)

    def test_inputaction4(self):
        self.e3.inqueue.put(Token(src='e2', dst='e3'))
        time.sleep(0.1)
        with mock.patch('time.sleep'):
            self.e3.loop()
        self.assertEqual(
            self.e4.inqueue.get(timeout=1), Token(src='e3', dst='e4')
        )
        self.assertEqual(
            self.e5.inqueue.get(timeout=1), TokenSent(src='e3', dst='e5')
        )

        self.e3.next_alive = False
        self.e3.inqueue.put(Token(src='e1', dst='e3'))
        time.sleep(0.1)
        with mock.patch('time.sleep'):
            self.e3.loop()
        self.assertEqual(
            self.e5.inqueue.get(timeout=1), Token(src='e3', dst='e5')
        )

    def test_inputaction5(self):
        with mock.patch.object(Timeout, 'expired', side_effect=(True, False)):
            self.e3.loop()

        self.assertTrue(
            self.e1.inqueue.get(timeout=1), TokenCreated(src='e3', dst='e1')
        )
        self.assertTrue(
            self.e4.inqueue.get(timeout=1), Token(src='e3', dst='e4')
        )
        self.assertTrue(
            self.e5.inqueue.get(timeout=1), TokenSent(src='e3', dst='e5')
        )
