#!/usr/bin/env python3

from async_algorithm.algorithm import Algorithm
from async_algorithm.utils import Ring
from task_4.tokenring import TokenRingNode

algo = Algorithm()

ring = Ring(15, 'e')

for pos in ring:
    name = str(pos)

    if name == 'e1':
        element = TokenRingNode(name, pos, n0=True, failafter=10)
    else:
        element = TokenRingNode(name, pos)

    algo.add_element(element)

for pos in ring:
    algo.add_connection(str(pos), str(pos + 1))
    algo.add_connection(str(pos), str(pos + 2))
    algo.add_connection(str(pos), str(pos - 2))

algo.run()
