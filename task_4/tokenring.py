import time

from async_algorithm.element import Element
from async_algorithm.message import Message, Nil
from async_algorithm.utils import Timeout


class Token(Message):
    pass


class TokenSent(Message):
    pass


class TokenCreated(Message):
    pass


class TokenRingNode(Element):
    def __init__(self, name, pos, n0=False, failafter=None):
        super().__init__(name, n0=n0, failafter=failafter)

        self.pos = pos
        self.next_alive = True

        self.timer = Timeout(5)

        self.inputactions = [
            (self.condition1, self.action1),
            (self.condition2, self.action2),
            (self.condition3, self.action3),
            (self.condition4, self.action4),
            (self.condition5, self.action5)
        ]

    @staticmethod
    def condition1(msg):
        return isinstance(msg, Nil)

    def action1(self, _):
        if self.n0:
            self.send_message(Token(dst=str(self.pos + 1)))
            self.send_message(TokenSent(dst=str(self.pos + 2)))

    def condition2(self, msg):
        return (
            isinstance(msg, TokenSent)
            and msg.src == str(self.pos - 2)
            and msg.dst == self.name
        )

    def action2(self, _):
        self.timer.start()

    def condition3(self, msg):
        return (
            isinstance(msg, TokenCreated)
            and msg.src == str(self.pos + 2)
            and msg.dst == self.name
        )

    def action3(self, _):
        self.next_alive = False

    def condition4(self, msg):
        return (
            isinstance(msg, Token)
            and msg.src in (str(self.pos - 1), str(self.pos - 2))
            and msg.dst == self.name
        )

    def action4(self, _):
        self.timer.reset()

        print('{}: Work in critical section.'.format(self.name))
        time.sleep(1)

        if self.next_alive:
            self.send_message(Token(dst=str(self.pos + 1)))
            self.send_message(TokenSent(dst=str(self.pos + 2)))
        else:
            self.send_message(Token(dst=str(self.pos + 2)))

    def condition5(self, _):
        return self.timer.expired()

    def action5(self, _):
        self.timer.reset()
        print('{}: Timeout expired, creating new token.'.format(self.name))
        self.send_message(Token(dst=str(self.pos + 1)))
        self.send_message(TokenSent(dst=str(self.pos + 2)))
        self.send_message(TokenCreated(dst=str(self.pos - 2)))
