# Idea for a simpy (DES) based implementation

import random
import simpy


class Node:
    def __init__(self, env, number, conn_in, conn_out, n0=False):
        self.env = env
        self.number = number
        self.n0 = n0
        self.conn_in = conn_in
        self.conn_out = conn_out

    def run(self):
        if self.n0:
            msg = 'token'
            self.conn_out.send(msg)
            print('Node {} intialized {} at {}.'.format(self.number, msg, self.env.now))


        while True:
            msg = yield self.conn_in.recieve()
            print('Node {} recieved {} at {}, sending to next node.'.format(self.number, msg, self.env.now))
            self.conn_out.send(msg)

class Connection:
    def __init__(self, env):
        self.env = env
        self.store = simpy.Store(env)

    def send(self, msg):
        self.env.process(self.delay(msg))

    def delay(self, msg):
        yield self.env.timeout(random.uniform(1, 3))
        self.store.put(msg)

    def recieve(self):
        return self.store.get()


#env = simpy.rt.RealtimeEnvironment()
env = simpy.Environment()

one2two = Connection(env)
two2three = Connection(env)
three2one = Connection(env)
nodes = [Node(env, 1, three2one, one2two, n0=True), Node(env, 2, one2two, two2three), Node(env, 3, two2three, three2one)]

for n in nodes:
    env.process(n.run())

env.run(until=60)
