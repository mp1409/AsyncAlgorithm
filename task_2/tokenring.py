import random
import time

from async_algorithm.element import Element
from async_algorithm.message import Message, Nil


class Load(Message):
    pass


class LoadGenerator(Element):
    def __init__(self, name, min_wait=3, max_wait=6):
        super().__init__(name)

        self.min_wait = min_wait
        self.max_wait = max_wait

    def loop(self):
        destination = random.choice(('c1', 'c2', 'c3'))
        self.send_message(Load(dst=destination))
        time.sleep(random.uniform(self.min_wait, self.max_wait))


class Token(Message):
    pass


class TokenRingNode(Element):
    def __init__(self, name, n0=False):
        super().__init__(name, n0=n0)

        self.cr_needed = False

        self.inputactions = [
            (self.condition1, self.action1),
            (self.condition2, self.action2),
            (self.condition3, self.action3)
        ]

    @staticmethod
    def condition1(msg):
        return isinstance(msg, Nil)

    def action1(self, _):
        if self.n0:
            print('{}: Nil message recieved, creating token.'.format(self.name))
            self.send_message(
                Token(dst=self._next_name_in_ring())
            )

    def condition2(self, msg):
        return (
            isinstance(msg, Load)
            and msg.src == 'LG'
            and msg.dst == self.name
        )

    def action2(self, _):
        self.cr_needed = True
        print('{}: CR needed.'.format(self.name))

    def condition3(self, msg):
        return (
            isinstance(msg, Token)
            and not (msg.src == 'LG' and msg.dst != self.name)
            and self.cr_needed
        )

    def action3(self, _):
        print('{}: Work in critical region, passing token.'.format(self.name))
        self.cr_needed = False
        self.send_message(Token(dst=self._next_name_in_ring()))

    def _next_name_in_ring(self):
        ring_names = ('c1', 'c2', 'c3')
        next_index = (ring_names.index(self.name) + 1) % len(ring_names)
        return ring_names[next_index]
