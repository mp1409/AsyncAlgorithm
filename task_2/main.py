#!/usr/bin/env python3

from async_algorithm.algorithm import Algorithm
from task_2.tokenring import TokenRingNode, LoadGenerator


algo = Algorithm()

algo.add_element(TokenRingNode('c1', n0=True))
algo.add_element(TokenRingNode('c2'))
algo.add_element(TokenRingNode('c3'))
algo.add_element(LoadGenerator('LG'))

algo.add_connection('c1', 'c2')
algo.add_connection('c2', 'c3')
algo.add_connection('c3', 'c1')

algo.add_connection('LG', 'c1')
algo.add_connection('LG', 'c2')
algo.add_connection('LG', 'c3')

algo.run()
