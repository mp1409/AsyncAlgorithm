import unittest
from unittest import mock

import time

from async_algorithm.algorithm import Algorithm
from async_algorithm.message import Nil
from task_2.tokenring import LoadGenerator, Load, TokenRingNode, Token


class TokenRingTestCase(unittest.TestCase):
    @staticmethod
    def _wait_and_loop(e):
        time.sleep(0.1)
        e.loop()

    def setUp(self):
        self.algo = Algorithm()

        self.c1 = TokenRingNode('c1', n0=True)
        self.c2 = TokenRingNode('c2')
        self.c3 = TokenRingNode('c3')

        self.lg = LoadGenerator('LG', min_wait=0, max_wait=0)

        self.algo.add_element(self.c1)
        self.algo.add_element(self.c2)
        self.algo.add_element(self.c3)
        self.algo.add_element(self.lg)

        self.algo.add_connection('c1', 'c2')
        self.algo.add_connection('c2', 'c3')
        self.algo.add_connection('c3', 'c1')

        self.algo.add_connection('LG', 'c1')
        self.algo.add_connection('LG', 'c2')
        self.algo.add_connection('LG', 'c3')

    def test_load_generator(self):
        for element in (self.c1, self.c2, self.c3):
            with mock.patch('random.choice', return_value=element.name) as m:
                self.lg.loop()
                m.assert_called_with(('c1', 'c2', 'c3'))
                self.assertEqual(
                    element.inqueue.get(timeout=1), Load('LG', element.name)
                )

    def test_next_name_in_ring(self):
        self.assertEqual(self.c1._next_name_in_ring(), 'c2')
        self.assertEqual(self.c2._next_name_in_ring(), 'c3')
        self.assertEqual(self.c3._next_name_in_ring(), 'c1')

    def test_inputaction1(self):
        self.c1.inqueue.put(Nil(dst=self.c1.name))
        self._wait_and_loop(self.c1)

        self.assertEqual(
            self.c2.inqueue.get(timeout=1), Token(src='c1', dst='c2')
        )

    def test_inputaction2(self):
        self.assertFalse(self.c1.cr_needed)
        self.c1.inqueue.put(Load(src='LG', dst='c1'))
        self._wait_and_loop(self.c1)
        self.assertTrue(self.c1.cr_needed)

    def test_inputaction3(self):
        self.c1.cr_needed = True
        self.c1.inqueue.put(Token(src='c3', dst='c1'))
        self._wait_and_loop(self.c1)

        self.assertFalse(self.c1.cr_needed)
        self.assertEqual(
            self.c2.inqueue.get(timeout=1), Token(src='c1', dst='c2')
        )
