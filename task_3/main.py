#!/usr/bin/env python3

from async_algorithm.algorithm import Algorithm
from task_3.berkley_time_sync import TimeServer, Client


algo = Algorithm()

algo.add_element(TimeServer('TS', ('c1', 'c2', 'c3'), n0=True))
algo.add_element(Client('c1', failafter=15))
algo.add_element(Client('c2'))
algo.add_element(Client('c3'))

algo.add_connection('c1', 'TS')
algo.add_connection('c2', 'TS')
algo.add_connection('c3', 'TS')

algo.add_connection('TS', 'c1')
algo.add_connection('TS', 'c2')
algo.add_connection('TS', 'c3')

algo.run()
