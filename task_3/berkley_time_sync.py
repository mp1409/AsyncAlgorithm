import random
import time
import datetime

from async_algorithm.element import Element
from async_algorithm.message import Message, Nil
from async_algorithm.utils import Timeout


class TimeRequest(Message):
    pass


class ClientTime(Message):
    def __init__(self, dest, my_time):
        super().__init__(dst=dest)

        self.time = my_time

    def __eq__(self, other):
        return super.__eq__(other) and self.time == other.time


class TimeCorrection(Message):
    def __init__(self, dest, time_diff):
        super().__init__(dst=dest)
        
        self.time_diff = time_diff

    def __eq__(self, other):
        return super.__eq__(other) and self.time_diff == other.time_diff

     
class TimeServer(Element):
    def __init__(self, name, clients, n0=False):
        super().__init__(name, n0=n0)

        self.inputactions = [
            (self.condition1, self.action1),
            (self.condition2, self.action2),
            (self.condition3, self.action3),
            (self.condition4, self.action4),
            (self.condition5, self.action5)
        ]

        self.clients = list(clients)
        self.responses = dict()
        self.syncTimer = Timeout(5)
        self.failTimer = Timeout(3)

    @staticmethod
    def condition1(msg):
        return isinstance(msg, Nil)

    def action1(self, _):
        if self.n0:
            print('{}: Nil message recieved, starting timer to synchronise clocks.'.format(self.name))
            self.syncTimer.start()

    """
    Input:  empty, check if it is time to start a new synchronisation
    Action: send a request to each client asking for their local time
    """
    def condition2(self, _):
        return self.syncTimer.expired()

    def action2(self, _):
        cprint('bold', '{}: sending time requests to all clients.'.format(
            self.name
        ))

        for client in self.clients:
            self.send_message(TimeRequest(dst=client))

        self.failTimer.start()
        self.syncTimer.reset()
        self.syncTimer.start()

    """ 
    Input:  time of one client
    Action: add time and client to response dictionary
    """    
    def condition3(self, msg):
        return (
            isinstance(msg, ClientTime)
            and msg.dst == self.name
        )

    def action3(self, msg):
        cprint('blue', '{}: got respsonse from Client {} -> {}'.format(
            self.name, msg.src, datetime.datetime.fromtimestamp(msg.time).time()
        ))
        self.responses[msg.src] = msg.time

    """
    Input:  empty, check if all clients responded
    Action: compute average time and send correction values to each client
    """    
    def condition4(self, _):
        return (
            len(self.responses) == len(self.clients)
        )

    def action4(self, _):
        print('{}: received time from all clients and sending correction values.'.format(self.name))
        self.failTimer.reset()
        
        avg_time = sum(self.responses.values()) / len(self.responses)

        for client in self.responses:
            self.send_message(
                TimeCorrection(client, avg_time - self.responses[client])
            )
            
        self.responses.clear()
        
    """
    Input:  empty, check if failstop-timeout expired
    Action: remove missing client from list
    """
    def condition5(self, _):
        return self.failTimer.expired()
        
    def action5(self, _):
        failed_clients = []
        for client in self.clients:
            if client not in self.responses:
                failed_clients.append(client)

        cprint('yellow', '{}: Client {} failed!'.format(
            self.name, ', '.join(failed_clients)
        ))
        
        for client in failed_clients:
            self.clients.remove(client)
            
        self.failTimer.reset()
        

class Client(Element):
    def __init__(self, name, n0=False, failafter=None):
        super().__init__(name, n0=n0, failafter=failafter)

        self.inputactions = [
            (self.condition1, self.action1),
            (self.condition2, self.action2),
            (self.condition3, self.action3)
        ]
        
        self.myTime = time.time()

    @staticmethod
    def condition1(msg):
        return isinstance(msg, Nil)

    def action1(self, _):
        if self.n0:
            print('{}: Nil message recieved, nothing to do so far.'.format(
                self.name)
            )

    def condition2(self, msg):
        return (
            isinstance(msg, TimeRequest)
            and msg.src == 'TS'
            and msg.dst == self.name
        )

    def action2(self, _):
        offset = random.normalvariate(0, 60)
        self.myTime = time.time() + offset

        print('{}: sending my time {} to TS.'.format(
            self.name, datetime.datetime.fromtimestamp(self.myTime).time()
        ))
        self.send_message(ClientTime('TS', self.myTime))

    def condition3(self, msg):
        return (
            isinstance(msg, TimeCorrection)
            and msg.src == 'TS' 
            and msg.dst == self.name
        )

    def action3(self, msg):
        self.myTime += msg.time_diff
        cprint('green', '{}: adjusted my time to {}.'.format(
            self.name, datetime.datetime.fromtimestamp(self.myTime).time()
        ))


def cprint(color, msg):
    colors = {
        'green': '\033[92m',
        'yellow': '\033[93m',
        'blue': '\033[94m',
        'bold': '\033[1m'
    }

    print(colors[color] + msg + '\033[0m')
