# AsyncAlgorithm [![build status](https://gitlab.com/mp1409/AsyncAlgorithm/badges/master/build.svg)](https://gitlab.com/mp1409/AsyncAlgorithm/commits/master)
AsyncAlgorithm ist ein Framework zur Modellierung von asynchronen verteilten Algorithmen, geschrieben in Python.

## Anforderungen
Es genügt eine halbwegs aktuelle Implementierung von [Python 3](https://www.python.org) (>= 3.3).
Abgesehen von der Python-Standardbibliothek werden keine Pakete benötigt.

Getestete Implementierungen:
* CPython 3.5
* CPython 3.4
* CPython 3.3
* PyPy3 5.5 (implementiert Python 3.3)
* CPython 3.6 (beta)

## Überblick
Dieses Repository enthält:
* Das eigentliche Framework im Ordner `async_algorithm`.
* Eine Tokenring-Anwendung im Ordner `task_2`.
* Eine Anwendung zur Berkley-Zeitsynchronisation im Ordner `task_3`.
* Eine Tokenring-Anwendung, die Fail-Stop-Ausfälle toleriert, im Ordner `task_4`.

## Verwendung des Framworks
Prinzipiell besteht ein verteilter Algorithmus aus verschiedenen Algorithmuselementen (mit Variablen und Input-Action-Statements), Verbindungen zwischen den Elementen und verschiedenen Botschaften.

### Platzierung des Quellcodes
Wenn der Quellcode des Algorithmus in einem Unterordner liegen soll (wie etwa bei `task_2`), muss der Hauptordner in der Regel vor dem Start zum Python-Pfad hinzugefügt werden.
Das kann zum Beispiel durch folgendes Start-Skript im Unterordner, hier mit dem Namen `run.sh`, geschehen:
```shell
#!/bin/sh

PYTHONPATH=$PYTHON_PATH:.. /usr/bin/env python3 main.py
```

Die Anwendung kann dann wie folgt gestartet werden:
```
cd task_2
./run.sh
```

### Der Algorithmus
Zunächst muss eine Instanz der Klasse `Algorithm` aus dem Modul `async_algorithm.algorithm` erstellt werden:
```python
from async_algorithm.algorithm import Algorithm

algo = Algorithm()
```

Danach werden die Algorithmuselemente hinzugefügt. Jedes Element erhält einen Namen und kann optional der Menge N0 hinzugefügt werden:
```python
algo.add_element(MyElement('e1', n0=True))
algo.add_element(MyElement('e2'))
algo.add_element(MyOtherElement('o1'))
```
Bei den Algortihmuselementen handelt es sich um Instanzen von Klassen, deren Definition im übernächsten Abschnitt erläutert wird.

Anschließend werden die Verbindungen definiert:
```python
algo.add_connection('e1', 'e2')
algo.add_connection('e2', 'e1')
algo.add_connection('o1', 'e2')
```

Zuletzt muss der Algorithmus nur noch gestartet werden:
```python
algo.run()
```
Der Ablauf kann mit Strg+C abgebrochen werden.

### Nachrichten
Die Implementierung von Nachrichten geschieht durch Erben von der Klasse `Message` im Modul `async_algorithm.message`:
```python
from async_algorithm.message import Message

class MyMessage(Message):
    pass
```

Die Nachrichten können dann wie folgt instanziert werden:
```python
MyMessage(src='e1', dst='e2')
```

Die nil-Message ist bereits durch die Klasse `Nil` im Modul `async_algorithm.message` implementiert.
Eine Instanz dieser Klasse wird durch das Framework beim Start jedem Element zugestellt.

### Elemente
Die einzelnen Elemente des Algorithmus können implementiert werden, in dem von der Klasse `Element` im Modul `async_algorithm.element` geerbt wird.
Beispiel:

```python
from async_algorithm.element import Element

class MyElement(Element):
    def __init__(self, name, n0=False):
        super().__init__(name, n0=n0)

        self.examplevar = False

        self.inputactions = [
            (self.condition1, self.action1)
        ]

    def condition1(self, msg):
        return (
            isinstance(msg, MyMessage)
            and self.examplevar == True
        )

    def action1(self, _):
        self.examplevar = False
        self.send_message(MyOtherMessage(dst='o1'))
```

Im Folgenden werden die Komponenten eines Elements genauer vorgestellt.

#### Konstruktor
Der Konstuktor muss dem Konstruktor der Oberklasse (`Element`) den Namen der Instanz übergeben.
Die Zugehörigkeit zu N0 kann ebenfalls an die Oberklasse durchgereicht werden, dies ist aber nicht zwingend erforderlich (etwa wenn Instanzen dieser Elementklasse niemals zu N0 gehören).

Dies gilt analog auch auch für den optionalen Parameter `failafter`.
Beispiel für ein Element, dass nie zu N0 gehört und nach einer zufälligen Zeit (zwischen 5 und 60 Sekunden) ausfällt:
```python
import random

from async_algorithm.element import Element

class MyElement(Element):
    def __init__(self, name):
        super().__init__(name, n0=False, failafter=random.randint(5, 60))

        # Rest der Klasse wie oben...
```

#### Elementvariablen
Variablen des Elements werden einfach als Instanzvariablen der Klasse angelegt (Im Beispiel: `self.examplevar`).

#### Input-Action-Statements
Input-Action-Statements werden als Tupel `(Bedingung, Aktion)` in der Liste `self.inputactions` abgelegt.

Die Bedingung ist eine Methode, die als Parameter eine Nachricht oder `None` erhält, und entweder `True` oder `False` zurückgibt.
Weil diese Methode nicht immer mit einer Nachricht ausgewertet wird (Auswertungen erfolgen auch zwischen zwei Nachrichten, um auf Variablenveränderungen reagieren zu können), muss die Bedingung auch `None` als Eingabe akzeptieren.
Dabei darf diese Methode keine Seiteneffekte besitzen (zum Beispiel dürfen keine Instanzvariablen verändert werden).
Dies ist auch im theoretischen Modell nicht erlaubt.

Die Aktion ist eine Methode mit demselben Parameter (Nachricht oder `None`) und Rückgabewert.

#### Weitere Möglichkeiten
Es steht eine `send_message`-Methode bereit, mit der Nachrichten versendet werden können.
Diese ergänzt auch den Absender der Nachricht automatisch, so dass dieser nicht angegeben werden muss.

Optional kann auch die `loop`-Methode überschrieben werden, wenn ein von der normalen Input-Action-Semantik abweichendes Verhalten gewünscht ist.
Die Loop-Methode wird in einer Endlosschleife über die gesamte Lebensdauer des Elements immer wieder ausgeführt.
In diesem Fall brauchen natürlich auch keinen Input-Action-Paare in `self.inputactions` hinterlegt werden.
Beispiel:
```python
def loop(self):
    self.send_message(MyMessage(src=self.name, dst='e1'))
    time.sleep(1)
```

### Timeout
Es steht im Modul `async_algorithm.utils` ein simpler Timeout zur Verfügung.
Beispiel:
```python
import time
from async_algorithm.utils import Timeout

self.to = Timeout(10)
self.to.expired() # -> False
self.to.start()
self.to.expired() # -> False
time.sleep(10)
self.to.expired() # -> True
self.to.reset()
self.to.expired() # -> False
```

### Ring
Ebenfalls im Modul `async_algorithm.utils` finden sich die Klassen `Ring` und `RingPosition` zur Darstellung einer ringförmigen Netztopologie:
```python
from async_algorithm.utils import Ring

r = Ring(5, 'e')
str(r) # -> '[e1, e2, e3, e4, e5]'
str(r[3]) # -> 'e3'
str(r[3] - 1) # -> 'e2'
str(r[3] + 3) # -> 'e1'
```

## Implementierung
Die Details zur Implementierung des Frameworks finden sich als Kommentare im Quellcode.