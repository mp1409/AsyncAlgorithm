import time


class Timeout:
    """ A simple timeout. Duration is in seconds. """

    def __init__(self, duration):
        self._duration = duration
        self._expires = None

    def start(self):
        """ Start the timeout. The timeout must not be running before. """
        if self._expires is None:
            self._expires = time.monotonic() + self._duration
        else:
            raise RuntimeError('Timeout already running.')

    def expired(self):
        """ Check if the timeout has expired. """
        return self._expires is not None and time.monotonic() > self._expires

    def reset(self):
        """ Reset the timeout. """
        self._expires = None


class Ring:
    """ Helper class for ring-shaped network topology. """

    def __init__(self, length, prefix):
        """
        Create a new ring of length with prefix. For example, length 3 and
        prefix 'e' gives the ring [e1, e2, e3].
        """
        self.length = length
        self.prefix = prefix

    def __eq__(self, other):
        return self.length == other.length and self.prefix == self.prefix

    def __iter__(self):
        for i in range(1, self.length + 1):
            yield RingPosition(self, i)

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        if 1 <= index <= self.length:
            return RingPosition(self, index)
        else:
            raise KeyError('Index not in ring!')

    def __str__(self):
        elements = ', '.join(str(e) for e in self)
        return '[{}]'.format(elements)


class RingPosition:
    """
    Represents a specific position in the ring. Following and succeding elements
    can be accessed via addition and subtraction of integers.
    """

    def __init__(self, ring, index):
        self.ring = ring
        self.index = index

    def __eq__(self, other):
        return self.ring == other.ring and self.index == other.index

    def __add__(self, other):
        newindex = ((self.index - 1 + other) % len(self.ring)) + 1
        return RingPosition(self.ring, newindex)

    def __sub__(self, other):
        return self + (-other)

    def __str__(self):
        return self.ring.prefix + str(self.index)
