class Message:
    """ This is an abstract message intended to be subclassed."""

    def __init__(self, src='', dst=''):
        self.src = src
        self.dst = dst

    def __str__(self):
        return '{}[{}->{}]'.format(self.__class__.__name__, self.src, self.dst)

    def __repr__(self):
        return '{}(src={}, dst={})'.format(
            self.__class__.__name__, self.src, self.dst
        )

    def __eq__(self, other):
        """
        This method overloads the == operator, and checks if type, source and
        destination of two messages are the same. Overwriting this method was
        necessary because after being piped through a queue, the ids of a
        message changes.

        If a subclass adds additional message fields as instance variables,
        those should be tested too.
        """
        return (
            type(self) == type(other) and self.src == other.src
            and self.dst == other.dst
        )


class Nil(Message):
    """ This class represents the Nil message type. """
    pass
