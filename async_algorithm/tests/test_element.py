import unittest

import time

from ..element import Element
from ..message import Message
from ..utils import Timeout


class ElementTestCase(unittest.TestCase):
    class SimpleElement(Element):
        def __init__(self, name, n0=False):
            super().__init__(name, n0=n0)
            self.inputactions = [(self.condition, self.action)]
            self.actions_done = 0

        @staticmethod
        def condition(msg):
            return isinstance(msg, Message)

        def action(self, _):
            self.actions_done += 1

    class InvalidElement(Element):
        def __init__(self, name):
            super().__init__(name)
            self.inputactions = [(self.condition, self.action)]
            self.action_done = False

        @staticmethod
        def condition(_):
            return False

        def action(self, _):
            self.action_done = True

    class TimeoutElement(Element):
        def __init__(self, name):
            super().__init__(name)
            self.inputactions = [
                (self.condition1, self.action1),
                (self.condition2, self.action2)
            ]

            self.timeout = Timeout(0.01)

            self.action_done = False

        @staticmethod
        def condition1(msg):
            return isinstance(msg, Message)

        def action1(self, _):
            self.timeout.start()

        def condition2(self, _):
            return self.timeout.expired()

        def action2(self, _):
            self.timeout.reset()
            self.action_done = True

    @staticmethod
    def _wait_and_loop(e):
        time.sleep(0.1)
        e.loop()

    def test_instantiation(self):
        with self.assertRaises(RuntimeError):
            self.SimpleElement('')

        e1 = self.SimpleElement('e1')
        e2 = self.SimpleElement('e2', n0=True)
        i1 = self.InvalidElement('i1')

        self.assertEqual(e1.n0, False)
        self.assertEqual(e2.n0, True)
        self.assertEqual(i1.n0, False)

    def test_simple_inputaction(self):
        e1 = self.SimpleElement('e1')
        e1.inqueue.put(Message(dst='e1'))

        self.assertEqual(e1.actions_done, 0)
        self._wait_and_loop(e1)
        self.assertEqual(e1.actions_done, 1)
        self.assertFalse(e1._messagebuffer)

    def test_two_inputactions(self):
        e1 = self.SimpleElement('e1')

        e1._messagebuffer.append(Message(dst='e1'))
        e1.inqueue.put(Message(dst='e1'))

        self.assertEqual(e1.actions_done, 0)
        self._wait_and_loop(e1)
        self.assertEqual(e1.actions_done, 2)
        self.assertFalse(e1._messagebuffer)

    def test_invalid_inputaction(self):
        i1 = self.InvalidElement('i1')
        i1.inqueue.put(Message(dst='i1'))

        self.assertFalse(i1.action_done)
        self._wait_and_loop(i1)
        self.assertFalse(i1.action_done)
        self.assertEqual(len(i1._messagebuffer), 1)

    def test_timeout(self):
        t1 = self.TimeoutElement('t1')

        t1.inqueue.put(Message(dst='t1'))
        self._wait_and_loop(t1)
        self.assertFalse(t1.action_done)
        self.assertFalse(t1._messagebuffer)

        time.sleep(0.01)
        self._wait_and_loop(t1)
        self.assertTrue(t1.action_done)
