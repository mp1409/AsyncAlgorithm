import unittest

from ..algorithm import Algorithm
from ..element import Element
from ..message import Message, Nil


class AlgorithmTestCase(unittest.TestCase):
    def setUp(self):
        self.algo = Algorithm()

        self.e1 = Element('e1')
        self.e2 = Element('e2', n0=True)
        self.isolated = Element('isolated')
        self.inactive = Element('inactive')

        self.algo.add_element(self.e1)
        self.algo.add_element(self.e2)
        self.algo.add_element(self.isolated)

        self.algo.add_connection('e1', 'e2')

    def test_add_element(self):
        self.assertEqual(
            self.algo.elements,
            {'e1': self.e1, 'e2': self.e2, 'isolated': self.isolated}
        )

    def test_element_access(self):
        self.assertEqual(self.algo['e1'], self.e1)
        self.assertEqual(self.algo['e2'], self.e2)
        self.assertEqual(self.algo['isolated'], self.isolated)

    def test_add_connection(self):
        self.assertEqual(self.algo.connections, [('e1', 'e2')])

        with self.assertRaises(RuntimeError):
            self.algo.add_connection('e1', 'invalid')

        with self.assertRaises(RuntimeError):
            self.algo.add_connection('invalid', 'e2')

        with self.assertRaises(RuntimeError):
            self.algo.add_connection('invalid', 'invalid')

        self.assertEqual(self.e1.outqueues, {'e2': self.e2.inqueue})

    def test_send_message(self):
        msg = Message(src='e1', dst='e2')
        self.e1.send_message(msg)
        self.assertEqual(self.e2.inqueue.get(timeout=1), msg)
        self.e1.send_message(Message(dst='e2'))
        self.assertEqual(self.e2.inqueue.get(timeout=1), msg)

        with self.assertRaises(RuntimeWarning):
            self.e1.send_message(Message(src='e2', dst='e2'))

        with self.assertRaises(RuntimeWarning):
            self.e1.send_message(Message(src='e1', dst='invalid'))
        with self.assertRaises(RuntimeWarning):
            self.e1.send_message(Message(src='e1', dst='isolated'))

        with self.assertRaises(RuntimeWarning):
            self.isolated.send_message(Message(dst='e1'))
        with self.assertRaises(RuntimeWarning):
            self.inactive.send_message(Message(dst='e1'))

    def test_send_nil_messages(self):
        self.algo._send_nil_messages()

        for element in (self.e1, self.e2, self.isolated):
            self.assertEqual(
                element.inqueue.get(timeout=1), Nil(dst=element.name)
            )
