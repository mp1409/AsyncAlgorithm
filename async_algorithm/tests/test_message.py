import unittest

from ..message import Message


class MessageTestCase(unittest.TestCase):
    class OtherMessage(Message):
        pass

    def test_equality(self):
        m1 = Message(src='e1', dst='e2')
        m2 = Message(src='e1', dst='e2')
        self.assertEqual(m1, m2)

        m3 = Message(src='e1', dst='o1')
        self.assertNotEqual(m1, m3)

        m4 = Message(src='o1', dst='e2')
        self.assertNotEqual(m1, m4)

        m5 = self.OtherMessage(src='e1', dst='e2')
        self.assertNotEqual(m1, m5)
