import unittest

import time

from ..utils import Timeout, Ring, RingPosition


class TimeoutTestCase(unittest.TestCase):
    def setUp(self):
        self.duration = 0.01
        self.to = Timeout(self.duration)

    def test_normal_operation(self):
        self.to.start()
        self.assertFalse(self.to.expired())
        time.sleep(self.duration)
        self.assertTrue(self.to.expired())

    def test_multiple_starts(self):
        self.to.start()
        with self.assertRaises(RuntimeError):
            self.to.start()


class RingTestCase(unittest.TestCase):
    def setUp(self):
        self.ring = Ring(5, 'e')

    def test_equality(self):
        self.assertEqual(self.ring, Ring(5, 'e'))

    def test_iteration(self):
        for i, pos in enumerate(self.ring, start=1):
            self.assertEqual(pos, RingPosition(self.ring, i))

    def test_len(self):
        self.assertEqual(len(self.ring), 5)

    def test_getitem(self):
        with self.assertRaises(KeyError):
            _ = self.ring[0]

        self.assertEqual(self.ring[1], RingPosition(self.ring, 1))
        self.assertEqual(self.ring[5], RingPosition(self.ring, 5))

        with self.assertRaises(KeyError):
            _ = self.ring[6]

    def test_str(self):
        self.assertEqual(str(self.ring), '[e1, e2, e3, e4, e5]')


class RingPositionTestCase(unittest.TestCase):
    def setUp(self):
        self.ring = Ring(5, 'e')
        self.ringpos1 = RingPosition(self.ring, 1)
        self.ringpos2 = RingPosition(self.ring, 2)
        self.ringpos3 = RingPosition(self.ring, 3)
        self.ringpos4 = RingPosition(self.ring, 4)
        self.ringpos5 = RingPosition(self.ring, 5)

    def test_equality(self):
        self.assertEqual(self.ringpos1, RingPosition(self.ring, 1))

    def test_plus(self):
        self.assertEqual(self.ringpos1 + 0, self.ringpos1)
        self.assertEqual(self.ringpos1 + 1, self.ringpos2)
        self.assertEqual(self.ringpos1 + 3, self.ringpos4)
        self.assertEqual(self.ringpos1 + 5, self.ringpos1)
        self.assertEqual(self.ringpos1 + 10, self.ringpos1)

        self.assertEqual(self.ringpos5 + 1, self.ringpos1)

        self.assertEqual(self.ringpos5 + (-1), self.ringpos4)
        self.assertEqual(self.ringpos5 + (-3), self.ringpos2)
        self.assertEqual(self.ringpos5 + (-5), self.ringpos5)
        self.assertEqual(self.ringpos5 + (-10), self.ringpos5)

        self.assertEqual(self.ringpos1 + (-1), self.ringpos5)

    def test_minus(self):
        self.assertEqual(self.ringpos5 - 0, self.ringpos5)
        self.assertEqual(self.ringpos5 - 1, self.ringpos4)
        self.assertEqual(self.ringpos5 - 3, self.ringpos2)
        self.assertEqual(self.ringpos5 - 5, self.ringpos5)
        self.assertEqual(self.ringpos5 - 10, self.ringpos5)

        self.assertEqual(self.ringpos1 - 1, self.ringpos5)

        self.assertEqual(self.ringpos1 - (-1), self.ringpos2)
        self.assertEqual(self.ringpos1 - (-3), self.ringpos4)
        self.assertEqual(self.ringpos1 - (-5), self.ringpos1)
        self.assertEqual(self.ringpos1 - (-10), self.ringpos1)

        self.assertEqual(self.ringpos5 - (-1), self.ringpos1)

    def test_str(self):
        self.assertEqual(str(self.ringpos1), 'e1')
