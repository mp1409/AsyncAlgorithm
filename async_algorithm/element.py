import multiprocessing
import queue
import random
import time


class Element(multiprocessing.Process):
    """
    This class represents an algorithm element.

    It is intended to be subclassed. The inputactions instance variable should
    be filled with tuples of condition/action method (see the README for
    details).

    Due to inheriting from Process, the class can also be run as a seperate
    process.
    """

    def __init__(self, name, n0=False, failafter=None):
        """
        Class constructor. The name will be used to address messages, n0
        indicates whether the element belongs to N0 and failafter (if specified)
        lets the process fail after the given number of seconds.
        """
        super().__init__()

        if name == '':
            raise RuntimeError('Element name must not be empty.')

        self.name = name
        self.n0 = n0

        if failafter is not None:
            self.failat = time.monotonic() + failafter
        else:
            self.failat = None

        self.inputactions = []

        self.inqueue = multiprocessing.Queue()
        self._messagebuffer = []
        self.outqueues = {}

    def send_message(self, msg):
        """
        Send a message to another element. If not already done, the source field
        of the message will be set to the current node. If source or destination
        are wrong, the message will be dropped and a warning will be emitted.
        """
        if msg.src == '':
            msg.src = self.name
        elif msg.src != self.name:
            raise RuntimeWarning(
                'Source for {} at node {} wrong, message dropped.'.format(
                    msg, self.name
                )
            )

        if msg.dst in self.outqueues:
            print('Message: {}'.format(msg))
            self.outqueues[msg.dst].put(msg)
        else:
            raise RuntimeWarning(
                'No link for message {}, message dropped.'.format(msg)
            )

    def loop(self):
        """
        The main loop body. By default, it evaluates the inputactions, but it
        can be overwritten if necessary.

        The evaluation follows a simple schema: Wait until a message arrives or
        a millisecond has passed. Keep a buffer of all unprocessed messages.
        Produce a list of all input actions whose condition is met. Select on
        action randomly, execute it and possibly remove the message from the
        messagebuffer. Continue until there are no more runnable actions.
        """
        try:
            msg = self.inqueue.get(timeout=0.001)
        except queue.Empty:
            pass
        else:
            self._messagebuffer.append(msg)

        while True:
            runnable_actions = []

            for msg in self._messagebuffer:
                for condition, action in self.inputactions:
                    if condition(msg):
                        runnable_actions.append((action, msg))

            for condition, action in self.inputactions:
                if condition(None):
                    runnable_actions.append((action, None))

            if not runnable_actions:
                break

            action, msg = random.choice(runnable_actions)
            if msg is not None:
                self._messagebuffer.remove(msg)
            action(msg)

    def run(self):
        """
        This method in invoked when the class is started as a process, and
        executes the loop() method over and over again.
        """
        try:
            while True:
                if self.failat is not None and time.monotonic() >= self.failat:
                    print('{} failed.'.format(self.name))
                    break

                self.loop()
        except KeyboardInterrupt:
            print('{} exiting.'.format(self.name))
