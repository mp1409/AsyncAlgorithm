from .message import Nil


class Algorithm:
    """
    This class represents an asynchronous distributed algorithm. The algorithm
    consists of several elements, which exchange messages.
    """

    def __init__(self):
        self.elements = {}
        self.connections = []

    def add_element(self, e):
        """ Add an element to the algorithm. """
        self.elements[e.name] = e

    def __getitem__(self, name):
        """ Get an algorithm by name via the [] operator. """
        return self.elements[name]

    def add_connection(self, name1, name2):
        """
        Add a (unidirectional) connection between the elements named name1 and
        name1.

        This has to happen before the algortihm's run() method is invoked,
        because after that the elements are seperate processes, and there is no
        way to access their private variables.
        """
        if name1 not in self.elements:
            raise RuntimeError('{} is no registered element.'.format(name1))
        if name2 not in self.elements:
            raise RuntimeError('{} is no registered element.'.format(name2))

        element1, element2 = self.elements[name1], self.elements[name2]
        element1.outqueues[name2] = element2.inqueue

        self.connections.append((name1, name2))

    def _send_nil_messages(self):
        """
        Send Nil messages to all nodes. This method has been moved out of run()
        to allow testing it without starting the processes.
        """
        for element in self.elements.values():
            nilmsg = Nil(dst=element.name)
            print('Message: {}'.format(nilmsg))
            element.inqueue.put(nilmsg)

    def run(self):
        """
        Start the algorithm. This means spawning the element processes and
        delivering the Nil messages. Afterwards, the algorithm process just has
        to wait for its children to finsh :-). The keyboard interrupts are
        propageted to the children, so we don't have to stop them explicitly.
        """
        try:
            print('Starting algorithm, quit with Ctrl+C.')
            print('=' * 70)

            for element in self.elements.values():
                element.start()

            self._send_nil_messages()

            for element in self.elements.values():
                element.join()

        except KeyboardInterrupt:
            for element in self.elements.values():
                element.join()

        print('=' * 70)
        print('All processes stopped.')
